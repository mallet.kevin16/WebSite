import { sleep } from 'k6';
import http from 'k6/http';

export let options = {
    duration: '1m',
    vus: 200,
};

export default function () {
    http.get('http://www.esme-testsurge.surge.sh');
    sleep(1);
}
